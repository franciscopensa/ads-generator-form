import React from 'react';
import styled from 'styled-components';
import Navbar from './components/Navbar';
import SloganCreator from './containers/SloganCreator';
import { SloganCreatorProvider } from './reducers/sloganCreator/reducer';

const Wrap = styled.div`
  margin: 0 auto;
  max-width: 1280px;
  padding-bottom: 50px;
  width: 100%;
`;

const App = () => {
  return (
    <React.Fragment>
      <Navbar />
      <Wrap>
        <SloganCreatorProvider>
          <SloganCreator />
        </SloganCreatorProvider>
      </Wrap>
    </React.Fragment>
  );
};

export default App;
