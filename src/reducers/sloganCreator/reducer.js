import React, { useReducer } from 'react';
import ACTIONS from './actions';

export const SloganCreatorStore = React.createContext('');

const initialState = {
  loadingSlogans: false,
  selectedSlogan: null,
  loadingDescriptions: false,
  selectedDescription: null,
  selectedWords: null,
  suggestedDescriptions: null,
  step: 1,
  suggestedSlogans: null,
  userBusinessDetails: null,
};

const reducer = (state, action) => {
  switch (action.type) {
    case ACTIONS.SUBMIT_BUSINESS_FORM:
      return {
        ...state,
        userBusinessDetails: action.data,
      };
    case ACTIONS.LOADING_SLOGANS:
      return {
        ...state,
        loadingSlogans: action.loading,
      };
    case ACTIONS.SET_MODEL_RESPONSE:
      const { suggestedDescriptions, suggestedSlogans } = action;
      return {
        ...state,
        suggestedSlogans,
        suggestedDescriptions,
      };
    case ACTIONS.SET_SUGGESTED_SLOGANS:
      return {
        ...state,
        suggestedSlogans: action.slogans,
      };
    case ACTIONS.SET_SELECTED_SLOGAN:
      return {
        ...state,
        selectedSlogan: action.slogan,
      };
    case ACTIONS.SET_SUGGESTED_DESCRIPTIONS:
      return {
        ...state,
        suggestedDescriptions: action.descriptions,
      };
    case ACTIONS.SET_SELECTED_DESCRIPTION:
      return {
        ...state,
        selectedDescription: action.description,
      };
    case ACTIONS.SET_SELECTED_DESCRIPTION_WORDS:
      return {
        ...state,
        selectedWords: action.selectedWords,
      };
    case ACTIONS.CREATOR_NEXT_STEP:
      return {
        ...state,
        step: state.step + 1,
      };
    case ACTIONS.JUMP_TO_STEP:
      return {
        ...state,
        step: action.step,
      };

    default:
      return { ...state };
  }
};

export function SloganCreatorProvider(props) {
  return (
    <SloganCreatorStore.Provider value={useReducer(reducer, initialState)}>
      {props.children}
    </SloganCreatorStore.Provider>
  );
}
