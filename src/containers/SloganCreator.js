import React, { useContext, useEffect } from 'react';
import { Divider, Steps } from 'antd';
import { LoadingOutlined } from '@ant-design/icons';

import SloganForm from '../components/SloganForm';
import { SloganCreatorStore } from '../reducers/sloganCreator/reducer';
import styled from 'styled-components';
import ACTIONS from '../reducers/sloganCreator/actions';
import CardSelector from '../components/CardSelector';
import SloganPreview from '../components/SloganPreview';

import Container from '../components/Container';

const STEPS_HEADER = [
  { title: 'Dados comerciais' },
  { title: 'Escolha seu slogan' },
  { title: 'Escolha sua descrição' },
  { title: 'Visualizar anúncio' },
];

const { Step } = Steps;

const StyledLoadingOutlined = styled(LoadingOutlined)`
  margin-right: 10px;
`;

const StyledDivider = styled(Divider)`
  margin-bottom: 30px;
`;
const StyledSteps = styled(Steps)`
  padding-top: 20px;
  margin-bottom: 50px;
  width: auto;

  .ant-steps-item-finish {
    .ant-steps-item-icon,
    .ant-steps-item-content {
      cursor: pointer;
    }
  }
`;
 
const BannerTitle = styled.div`
  margin: 0 auto;
  margin-bottom: 50px;
  text-align: center;
  max-width: 650px;

  h1 {
    font-size: 24px;
  }

  h2 {
    font-size: 16px;
  }
`;
 
const SloganCreator = () => {
  const [state, dispatch] = useContext(SloganCreatorStore);

  
  useEffect(() => {
    const getSlogans = async ({ revalidate } = {}) => {
      dispatch({ loading: true, type: ACTIONS.LOADING_SLOGANS });
  
      const {
        business_name,
        business_description,
        city,
        category,
        model
      } = state?.userBusinessDetails;
      
      const reqUrl = 'https://natural.do/ad-generator/generate';
      const body = { business_name, business_description, city, category, model };
  
      fetch(reqUrl, {
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(body),
        method: 'POST',

      })
        .then((response) => response.json())
        .then((data) => {
          if (data) {
            dispatch({
              suggestedDescriptions: data?.descripciones || state.suggestedDescriptions,
              suggestedSlogans: data?.slogans,
              type: ACTIONS.SET_MODEL_RESPONSE,
            });
  
            dispatch({ loading: false, type: ACTIONS.LOADING_SLOGANS });
  
            if (!revalidate) {
              dispatch({
                type: ACTIONS.CREATOR_NEXT_STEP,
              });
            }
          }
        })
        .catch((error) => {
          dispatch({ loading: false, type: ACTIONS.LOADING_SLOGANS });
          console.error('Error:', error);
        });
    };
    if (!state?.userBusinessDetails) return;
    getSlogans();
  }, [state?.userBusinessDetails]);

  return (
    <Container>
      <React.Fragment>
        <BannerTitle>
          <h1>Ad Generator</h1>
          <h2>
            Algorithm based on GPT-2 trained for Brazilian Portuguese, fine-tuned for
            advertising vertical and flavored with SentiLecto PT NLG capabilities
          </h2>
        </BannerTitle>
        <StyledSteps current={state.step - 1}>
          {STEPS_HEADER.map(({ title }, idx) => (
            <Step
              key={title}
              icon={idx === 0 && state?.loadingSlogans && <StyledLoadingOutlined />}
              onClick={() =>
                state.step > idx + 1 &&
                dispatch({ type: ACTIONS.JUMP_TO_STEP, step: idx + 1 })
              }
              title={title}
            />
          ))}
        </StyledSteps>

        <StyledDivider />

        {state.step === 1 && <SloganForm loading={state?.loadingSlogans} />}

        {state.step === 2 && (
          <CardSelector
            step={state.step}
            allowedToContinue={!!state?.selectedSlogan}
            disabled={state?.loadingSlogans}
            itemSelected={state?.selectedSlogan}
            list={state?.suggestedSlogans}
            onNextClick={() => dispatch({ type: ACTIONS.CREATOR_NEXT_STEP })}
            onSelect={(slogan) => dispatch({ slogan, type: ACTIONS.SET_SELECTED_SLOGAN })}
          />
        )}

        {state.step === 3 && (
          <CardSelector
            step={state.step}
            allowedToContinue={!!state?.selectedDescription}
            itemSelected={state?.selectedDescription}
            list={state?.suggestedDescriptions}
            onNextClick={() => dispatch({ type: ACTIONS.CREATOR_NEXT_STEP })}
            onSelect={(description) =>
              dispatch({ description, type: ACTIONS.SET_SELECTED_DESCRIPTION })
            }
          />
        )}

        {state.step === 4 && <SloganPreview />}
      </React.Fragment>
    </Container>
  );
};

export default SloganCreator;
