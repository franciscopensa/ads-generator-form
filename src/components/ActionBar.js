import React from 'react';
import styled from 'styled-components';

const Bar = styled.div`
  display: flex;
  justify-content: center;
`;

const ActionBar = ({ children }) => <Bar>{children}</Bar>;

export default ActionBar;
