import React, { useState, useContext } from 'react';
import styled from 'styled-components';
import { Card, Select } from 'antd';
import { SloganCreatorStore } from '../reducers/sloganCreator/reducer';
import ACTIONS from '../reducers/sloganCreator/actions';

const StyledCard = styled(Card)`
  ${({ disabled }) => disabled && 'pointer-events: none'};
  ${({ isSelected }) => isSelected && `border: solid 1px #04bde6`};
  ${({ isSelected }) => isSelected && `box-shadow: inset 0 0 0px 1px #04bde6`};
  position: relative;

  .ant-select {
    font-size: 18px;
  }

  .ant-select-selector {
    border-bottom: solid 1px #d9d9d9 !important;
  }

  &:hover {
    /* color: slateblue; */
  }
`;

const CardContent = styled.div`
  height: 100%;
  font-size: 18px;
`;

const Textbox = styled.div`
  display: inline;
  line-height: 36px;
`;

const CardInner = ({ id, content, disabled, itemSelected, onCardClick }) => {
  const [parsedText, setParsedText] = useState(
    content.map(({ text, select }) => [text, select?.[0].value].join(' ')).join(' ')
  );
  const [state, dispatch] = useContext(SloganCreatorStore);

  // console.log(content);

  const changeText = ({ index, itemSelected, value }) => {
    const newText = content
      .map(({ text, select }, idx) =>
        [text, idx === index ? value : select?.[0]?.value].join(' ')
      )
      .join(' ');

    setParsedText(newText);

    let selectedWords = state.selectedWords
    let index_val = content[index]['select'].findIndex(val => val['value'] === value)
    selectedWords[id][index] = index_val
    dispatch({ selectedWords, type: ACTIONS.SET_SELECTED_DESCRIPTION_WORDS })

    if (itemSelected) {
      onCardClick({ id, text: newText });
    }
  };

  return (
    <StyledCard
      disabled={disabled}
      hoverable={!itemSelected}
      isSelected={itemSelected}
      key={id}
      onClick={() => onCardClick({ id, text: parsedText })}
    >
      <CardContent onClick={() => onCardClick({ id, text: parsedText })}>
        {content?.map(({ text, select }, index) => (
          <Textbox>
            {text && <span className="textbox__text">{text}</span>}
            {select && (
              <Select
                bordered={false}
                defaultValue={select[state.selectedWords[id][index]]?.value}
                onClick={(e) => e.stopPropagation()}
                onChange={(value) => changeText({ itemSelected, index, value })}
                options={select}
              />
            )}
          </Textbox>
        ))}
      </CardContent>
    </StyledCard>
  );
};

export default CardInner;
