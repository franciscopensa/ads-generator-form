import React from 'react';
import styled from 'styled-components';

const Wrap = styled.div`
  max-width: 1280px;
  margin: 0 auto;
  padding: 0 15px;

  @media (max-width: 1024px) {
    max-width: 100%;
    padding: 0 15px;
  }
`;

const Container = ({ children }) => <Wrap>{children}</Wrap>;

export default Container;
