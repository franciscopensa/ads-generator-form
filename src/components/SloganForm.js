import React, { useContext } from 'react';
import styled from 'styled-components';
import { Button, Input, Select, TreeSelect } from 'antd';

import { Controller, useForm } from 'react-hook-form';
import categoryMock from '../static/shop_categories.json';
import locationsMock from '../static/locations.json';
import { SloganCreatorStore } from '../reducers/sloganCreator/reducer';
import ACTIONS from '../reducers/sloganCreator/actions';

const { TextArea } = Input;

const Wrap = styled.div`
  display: grid;
  grid-row-gap: 30px;
  grid-template-rows: 1fr;
  margin: 0 auto;
  max-width: 650px;

  .submit-button {
    margin-top: 30px;
  }
`;

const StyledInput = styled(Input)`
  ${({ hasError }) => hasError && `border: 1px solid #ffa39e`};
`;

const StyledTreeSelect = styled(TreeSelect)`
  .ant-select-selector {
    ${({ hasError }) => hasError && `border: 1px solid #ffa39e!important`};
  }
`;
const StyledSelect = styled(Select)`
  width: 300px;
  position: fixed;
  top: 10px;
  opacity: 0;
  &:hover {
    opacity: 1;
  }
`;
const FormGroup = styled.div`
  .ant-select.ant-tree-select {
    width: 500px;
  }
`;

const INPUTS = [
  {
    component: StyledInput,
    name: 'business_name',
    placeholder: 'Nome para sua loja (obrigatório)',
    rules: { required: true },
  },
  {
    component: TextArea,
    name: 'business_description',
    placeholder: 'Produto destacado (opcional)',
  },
  {
    component: StyledTreeSelect,
    name: 'category',
    otherProps: {
      showSearch: true,
      allowClear: true,
      treeData: categoryMock,
      style: { width: '100%' },
      virtual: false
    },
    placeholder: 'Categoria (obrigatório)',
  },
  {
    component: StyledInput,
    name: 'business_url',
    placeholder: 'Url da loja',
  },
  {
    component: Select,
    name: 'city',
    otherProps: {
      allowClear: true,
      options: locationsMock,
      showSearch: true,
      style: { width: '100%' },
    },
    placeholder: 'Cidade do Brasil ou bairro de SP (opcional)',
  },
];

const SloganForm = ({ loading }) => {
  const [state, dispatch] = useContext(SloganCreatorStore);
  const { control, errors, handleSubmit } = useForm({
    mode: 'onBlur',
  });
  const onFormSubmit = (data) => {
    dispatch({
      data,
      type: ACTIONS.SUBMIT_BUSINESS_FORM,
    });
  };

  return (
    <React.Fragment>
      <Wrap>
        {INPUTS.map(({ component: Component, name, otherProps, placeholder, rules }) => (
          <FormGroup key={name}>
            <Controller
              defaultValue={state?.userBusinessDetails?.[name] || null}
              as={
                <Component
                  disabled={loading}
                  hasError={errors?.[name]}
                  placeholder={placeholder}
                  name={name}
                  size="large"
                  {...otherProps}
                />
              }
              control={control}
              name={name}
              rules={rules}
            />
          </FormGroup>
        ))}

        <Button
          className="submit-button"
          disabled={loading}
          loading={loading}
          onClick={handleSubmit(onFormSubmit)}
          size="large"
          type="primary"
        >
          Gerar Slogans
        </Button>
        <FormGroup key='model'> 
            <Controller
              defaultValue={state?.userBusinessDetails?.['model'] || 'default'}
              as={
                <StyledSelect
                  name={'model'}
                  size="large">
                    <Select.Option value='default'>Default</Select.Option>
                    <Select.Option value='cb-1200'>Comida y bebida 1200 (Producción)</Select.Option>
                    <Select.Option value='cb-2800'>Comida y bebida 2800</Select.Option>
                    <Select.Option value='moda-full'>Moda</Select.Option>
                    <Select.Option value='tecno'>Tecnología</Select.Option>
                    <Select.Option value='all-full'>ALL Full</Select.Option>
                </StyledSelect>
              }
              control={control}
              name={'model'}
            />
          </FormGroup>
      </Wrap>
    </React.Fragment>
  );
};

export default SloganForm;
