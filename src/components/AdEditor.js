import React, { useState } from 'react';
import styled from 'styled-components';

import { EditOutlined } from '@ant-design/icons';

const AdBox = styled.div`
  border-radius: 6px;
  box-shadow: 0 1px 2px rgba(0, 0, 0, 0.04), 0 2px 4px rgba(0, 0, 0, 0.04),
    0 4px 8px rgba(0, 0, 0, 0.04), 0 8px 16px rgba(0, 0, 0, 0.04),
    0 16px 32px rgba(0, 0, 0, 0.04), 0 32px 64px rgba(0, 0, 0, 0.04);
  margin: 0 auto;
  margin-bottom: 50px;
  padding: 30px 50px;
  width: 70%;
`;

const AdWrap = styled.div`
  width: 600px;
  margin: 0 auto;
`;

const Header = styled.div`
  align-items: center;
  font-family: arial, sans-serif;
  display: flex;

  .header__ad-notice {
    align-items: center;
    background-color: #fff;
    color: #202124;
    display: flex;
    font-size: 14px;
    font-weight: bold;
    margin-left: 0px;
    white-space: nowrap;

    &:after {
      content: '·';
      padding: 0 5px;
    }
  }

  .header__site-url {
    color: #202124;
    font-size: 14px;
    white-space: nowrap;
  }
`;

const Description = styled.div`
  color: #4d5156;
  font-size: 14px;
  line-height: 1.58;

  .description {
    transition: all 200ms;
    &__edit-icon {
      position: relative;
      right: 10px;
    }

    &__edit {
      border-radius: 4px;
      border: none;
      padding: 4px 11px;
      position: relative;
      outline: none;
      left: -11px;
      resize: none;
      width: 100%;

      &:hover,
      &:focus {
        box-shadow: 0 0 4px #c6c6c6;

        + .description__edit-icon {
          opacity: 0;
          visibility: hidden;
        }
      }
    }
  }
`;

const Title = styled.h1`
  align-items: center;
  color: #1a0dab;
  display: flex;
  font-size: 20px;
  line-height: 26px;
  margin-bottom: 3px;
  padding-top: 3px;

  .title {
    position: relative;
    transition: all 200ms;

    &__business-name {
      flex: 0 0 auto;
    }

    &__edit-icon {
      font-size: 13px;
      position: relative;
      /* opacity: 0; */
      /* visibility: hidden; */
    }
    &__no-padding {
      padding-left: 0px!important;
    }

    &__slogan-edit {
      border-radius: 4px;
      outline: none;
      overflow: hidden;
      padding: 4px 11px;
      text-overflow: ellipsis;
      white-space: nowrap;

      &:hover,
      &:focus {
        box-shadow: 0 0 4px #c6c6c6;

        + .title__edit-icon {
          opacity: 0;
          visibility: hidden;
        }
      }
    }
  }
`;

const AdEditor = ({ businessName, businessUrl, defaultSlogan, defaultDescription }) => {
  const [slogan, setSlogan] = useState(defaultSlogan);
  const [description, setDescription] = useState(defaultDescription);
  const sloganHasName= slogan.includes(businessName)
  return (
    <React.Fragment>
      <AdBox>
        <AdWrap>
          <Header>
            <div className="header__ad-notice">Anúncio</div>
            <div className="header__site-url">{businessUrl || 'www.suaempresa.net/'}</div>
          </Header>

          <Title>
            {!sloganHasName && <span className="title title__business-name">{`${businessName} | `}</span>}
            <span
              className={sloganHasName ? "title title__slogan-edit title__no-padding" : "title title__slogan-edit"}
              contentEditable
              onChange={(e) => setSlogan(e.value)}
              role="textbox"
            >
              {slogan}
            </span>
            <EditOutlined className="title title__edit-icon" />
          </Title>

          <Description>
            <textarea
              className="description__edit"
              contentEditable
              maxLength="160"
              noresize
              onChange={(e) => setDescription(e.value)}
              rows="4"
              value={description}
            />
          </Description>
        </AdWrap>
      </AdBox>
    </React.Fragment>
  );
};

export default AdEditor;
