import React, { useContext } from 'react';
import { SloganCreatorStore } from '../reducers/sloganCreator/reducer';
import AdEditor from './AdEditor';

const SloganPreview = () => {
  const [state] = useContext(SloganCreatorStore);
  const { userBusinessDetails, selectedSlogan, selectedDescription } = state;
  const { business_name, business_url } = userBusinessDetails;

  return (
    <React.Fragment>
      <AdEditor
        businessName={business_name}
        businessUrl={business_url}
        defaultSlogan={selectedSlogan?.text}
        defaultDescription={selectedDescription?.text}
      />
      {/* <ActionBar>
        <Button type="primary" size="large">
          Finalizar
        </Button>
      </ActionBar> */}
    </React.Fragment>
  );
};

export default SloganPreview;
