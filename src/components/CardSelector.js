import { Button } from 'antd';
import React, { useEffect, useState, useContext } from 'react';
import { SloganCreatorStore } from '../reducers/sloganCreator/reducer';
import ACTIONS from '../reducers/sloganCreator/actions';
import styled from 'styled-components';
import CardInner from './CardInner';

const CardGrid = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr;
  grid-gap: 15px;
  margin-bottom: 50px;
`;

const ActionBar = styled.div`
  display: grid;
  grid-auto-flow: column;
  grid-column-gap: 15px;
  justify-content: flex-end;
`;

const optionPattern = /[^{{]+(?=}\})/g;

const CardSelector = ({
  step,
  allowedToContinue,
  disabled,
  list: cardList,
  onNextClick,
  onSelect,
  itemSelected,
  secondaryAction,
}) => {
  const [state, dispatch] = useContext(SloganCreatorStore);
  const [list, setList] = useState(null);

  useEffect(() => {
    const processedList = cardList.map(({ id, text: str }) => {
      const options = [...str.matchAll(optionPattern)];

      if (!options.length) {
        return { id, content: [{ text: str }] };
      }

      let lastRegStringStart;
      let textRest;

      const items = options.map((match, index) => {
        let item;
        const selectOptions = [...match][0].split('|');
        const select = selectOptions.map((value) => ({ label: value, value }));

        if (index === 0) {
          lastRegStringStart = match.index + [...match][0].length + 3;
          const text = str.substr(0, match.index - 3);
          item = { select, text };
        }

        if (index > 0) {
          const text = str.substr(
            lastRegStringStart,
            match.index - 3 - lastRegStringStart
          );

          lastRegStringStart = match.index + [...match][0].length + 3;

          item = { select, text };
        }

        if (index + 1 === options.length) {
          textRest = str.substr(lastRegStringStart, str.length);
        }

        return item;
      });

      return { id, content: [...items, { text: textRest }] };
    });

    if (!state.selectedWords && step === 3){
      let selectedWords = processedList.map(guion => guion['content'].filter(item => 'select' in item).map(() => 0))
      console.log('setea a cero', state, selectedWords, processedList)
      dispatch({ selectedWords, type: ACTIONS.SET_SELECTED_DESCRIPTION_WORDS })
    }

    setList(processedList);
    // eslint-disable-next-line
  }, []);

  return (
    <React.Fragment>
      <CardGrid>
        {list?.map((item) => (
          <CardInner
            disabled={disabled}
            content={item?.content}
            id={item?.id}
            itemSelected={item?.id === itemSelected?.id}
            onCardClick={onSelect}
          />
        ))}
      </CardGrid>

      <ActionBar>
        {secondaryAction && secondaryAction}
        <Button
          disabled={disabled || !allowedToContinue}
          onClick={onNextClick}
          size="large"
          type="primary"
        >
          Próximo
        </Button>
      </ActionBar>
    </React.Fragment>
  );
};

export default CardSelector;
